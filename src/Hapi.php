<?php

namespace Drupal\harvestapi;



class Hapi {

  var $bearer;
  var $account;
  var $headers = array();
  var $agent = 'CommonMedia Internal API';

  function __construct(){
    $harvest_config = \Drupal::config('system.harvestapi');    
    $this->setBearer($harvest_config->get('harvest.bearer'));
    $this->setAccount($harvest_config->get('harvest.account'));    
  }
  
  function setBearer($bearer) {
    $this->bearer = $bearer;
  }

  function setAccount($account) {
    $this->account = $account;
  }

  function getBearer() {
    return $this->bearer;
  }

  function getAccount() {
    return $this->account;
  }

  function getHeaders() {
    return array(
      "Authorization: Bearer " . $this->bearer,
      "Harvest-Account-ID: " . $this->account
    );
  }

  
  function getTimeEntries($from = NULL, $to = NULL, $page = 1, $billable = TRUE) {
    $parameters = array();

    if (is_string($from)) {
      $parameters['from'] = $from;
    }

    if (is_string($to)) {
      $parameters['to'] = $to;
    }

    if (is_integer($page)) {
      $parameters['page'] = $page;
    }
    
    if($billable){
      $parameters['is_billed'] = $billable ? 'true' : 'false';
    }

    
    return $this->api_request('https://api.harvestapp.com/v2/time_entries', $parameters);
  }

  function getLastMonthEntries() {
    $from = date('Y-m-d', strtotime('first day of last month'));
    $to = date('Y-m-d', strtotime('last day of last month'));
    return $this->getTimeEntries($from, $to);
  }

  /**
   * create a request on the api
   * 
   * @param type $url send just the url no "?"
   * @param type $parameters array of all parameters
   * @return type
   */
  function api_request($url = 'https://api.harvestapp.com/v2/time_entries', $parameters = array()) {
    
    $full_url = count($parameters) ?  "$url?" . http_build_query($parameters) :  $url;
    $handle = curl_init();
    curl_setopt($handle, CURLOPT_URL, $full_url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($handle, CURLOPT_HTTPHEADER, $this->getHeaders());
    curl_setopt($handle, CURLOPT_USERAGENT, $this->agent);
    $response = curl_exec($handle);
    if (curl_errno($handle)) {
      return "Error: " . curl_error($handle);
    }
    else {
      curl_close($handle);
      return json_encode(json_decode($response), JSON_PRETTY_PRINT);
    }
  }
  
  
  function getClients(){
    return $this->api_request('https://api.harvestapp.com/v2/clients');
  }

}
