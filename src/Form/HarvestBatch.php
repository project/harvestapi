<?php

namespace Drupal\harvestapi\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\harvestapi\Hapi;

class HarvestBatch extends FormBase {

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Execute an importing process for harvest time entry depending on the options below'),
    ];
    $form['batch'] = [
      '#type' => 'radios',
      '#title' => 'Time Entries To Process',
      '#options' => [
        'batch_1' => $this->t('Last Month Entries'),
        'batch_2' => $this->t('This Month Entries'),
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Process',
    ];

    return $form;
  }

  public function getFormId() {
    return 'harvestapi_batch';
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValues()['batch'];

    // Set the batch, using convenience methods.
    $batch = [];
    switch ($value) {
      case 'batch_1':
        $batch = $this->lastMonthBatch();
        break;

      case 'batch_2':
        $batch = $this->generateBatch2();
        break;
    }
    
    batch_set($batch);
  }

  public function lastMonthBatch() {
    $hapi = new Hapi();

    $start = TRUE;
    $entries = json_decode($hapi->getLastMonthEntries(), TRUE);
    $operations = [];

    while (!is_null($entries['links']['next']) || $start) {
      $start = FALSE;
      
      foreach ($entries['time_entries'] as $entry) {
        $operations[] = [
          'batch_time_entries_last_month',
          [
            'entry' => $entry
          ],
        ];
      }

      $entries = json_decode($hapi->api_request($entries['links']['next']), TRUE);
    }

    $this->messenger()->addMessage($this->t('Creating an array of @num operations', ['@num' => count($operations)]));
    $batch = [
      'title' => $this->t('Creating an array of @num operations', ['@num' => count($operations)]),
      'operations' => $operations,
      'finished' => 'batch_harvestapi_finished',
    ];
    return $batch;
  }

}
