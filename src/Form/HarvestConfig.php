<?php

namespace Drupal\harvestapi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

class HarvestConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['system.harvestapi'];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('system.harvestapi');
    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Get info for this form ') . Link::fromTextAndUrl(t('here'), Url::fromUri('https://id.getharvest.com/developers'))->toString(),
    ];

    $form['bearer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bearer'),
      '#description' => $this->t('Text provided on the api for Bearer'),
      '#default_value' => $config->get('harvest.bearer'),
      '#required' => TRUE,
    ];

    $form['account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account'),
      '#description' => $this->t('Text provided on the api for Account'),
      '#default_value' => $config->get('harvest.account'),
      '#required' => TRUE,
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  public function getFormId() {
    return 'harvestapi_configuration';
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('system.harvestapi')
        ->set('harvest.account', $form_state->getValue('account'))
        ->set('harvest.bearer', $form_state->getValue('bearer'))
        ->save();
    
    $this->messenger()->addMessage($this->t('All Values Saved Properly.'));
  }

}
